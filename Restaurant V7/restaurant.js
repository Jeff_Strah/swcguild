function validation(){
	var name = document.forms["infoForm"]["myName"].value;
	var email = document.forms["infoForm"]["myEmail"].value;
	var phone = (document.getElementById("myPhone")).value;
	var reason = (document.getElementById("myReason")).value;
	var info = (document.getElementById("myInfo")).value;
	var been = (document.getElementById("iBeen")).checked; 
	var noBeen = (document.getElementById("iNoBeen")).checked;
	var dayM = (document.getElementById("dayM")).checked;
	var dayTue = (document.getElementById("dayTue")).checked;
	var dayW = (document.getElementById("dayW")).checked;
	var dayThu = (document.getElementById("dayThu")).checked;
	var dayF = (document.getElementById("dayF")).checked;

		
		if (name === null || name === ""){	
			alert("Please enter a name.");
		}
		if (email === null || email === ""){
			alert("Please enter your email address.");
		}
		if (phone === null || phone === ""){
			alert("Please enter your phone number.");
		}
		if (reason === "Other" &&
			info === "" || info === null){
			alert("If reason is not provided in drop down menu, please explain your need.");
		}
		if (been === false && noBeen === false) {
			alert("Please let us know if you've been before.");
		}
		if (dayM === false &&
			dayTue === false &&
			dayW === false &&
			dayThu === false &&
			dayF === false){ 
			alert("Please at least let us contact you SOME time!");
		}
		
	}